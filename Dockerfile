FROM node:7-alpine

MAINTAINER https://gitlab.com/techunter.chaos/openshift-nodejs

RUN npm install -g karma gulp grunt typescript webpack protractor
RUN webdriver-manager update

COPY ./s2i/ $STI_SCRIPTS_PATH
RUN mkdir -p /opt/s2i/destination && \
    chown -R 1000:0 $HOME && \
    chgrp -R 0 /opt/s2i/destination && \
    chown -R 1000:0 /opt/s2i/destination && \
    chmod -R g+rw /opt/s2i/destination

USER 1000
WORKDIR /opt/s2i/destination
CMD $STI_SCRIPTS_PATH/usage